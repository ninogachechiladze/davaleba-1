import kotlin.math.sqrt

fun main(){
    /** toString მეთოდის შემოწმება */
    val k=Point(2.0,5.0)
    println("გადავიყვანოთ K(2.0,5.0) წერტილის მნიშვნელობა სთრინგში: ")
    println(k.toString())

    val j=Point(2.0,5.0)
    val rigitiPoint=Point(2.0,5.0)

    /** equals მეთოდის შემოწმება */

    println("შევამოწმოთ ორი წერტილი j და rigitiPoint, ერთიდაიგივე კოორდინატებით თუ გახდება იქუალს გადაწერილ ფუნქციაზე: " )
    println(j.equals(k))

    /** 0ის მიმართ სიმეტრიული წერტილის პოვნის მეთოდის შემოწმება */

    println("K წერტილის კოორდინატები 2.0,5.0-ია, მისი სიმეტრიული წერტილის სათავის მიმართ კი:")
    println( k.symetrivWithZero())

    /** ორწერტილს შორის მანძილის პოვნის მეთოდის შემოწმება */

    println("ავიღოთ ისევ j(2.0,5.0) და rigitiPoint(2.0,5.0).ვიპოვოთ მანძილი მათ შორის:")
    println(j.findDistanceTo(rigitiPoint))

    /**  ყველაფერი კანფეტივით მუშაობს წესით. ორ ფოინთს შორის მანძილი ასე გავაკეთე რომ პირველის სახელით
     * ვიწყებდე, ვიყენებდე კლასის ფუნქციას და მეორე იყოს ფუნქციის ინფუთი. იმედია
     * ეს სახე ამ ფუნქციის მისაღებია. */


    val wiladi1=Fraction(3,8)
    val wiladi25=Fraction(3,4)
    /** წილადის შეკრების მეთოდის შემოწმება.შეკრება ხდება უმცირესი საერთო მნიშვნელის პოვნის შედეგად  */
    println("შევკრიბოთ წილადები 3/8 და 3/4:")
    println(wiladi1.sum(wiladi25))

    val wiladikvecadi=Fraction(8,80)
    /** შეკვეცის ფუნქციის მეთოდის შემოწმება.ავტომატურად კვეცავს წილადს მნიშვნელისა და მრიცხველის უდიდეს გამყოფზე. */
    println("შევკვეცოთ წილადი 8/80 მნიშვნელის და მრიცხველის უ.ს.გ-ზე:")
    println(wiladikvecadi.shekvecaudidesGamyofze())

    /** გამრავლებაზე დავწერე ორი ფუნქცია, ერთი როდესაც წილადზე ვამრავლებთ წილადს,
     * მეორე როდესაც ინთეჯერ ტიპზე გვინდა გამრავლება*/
    val wiladigamravgayof1=Fraction(2,5)
    val wiladigarmravgayof2=Fraction(3,15)
    val gasamravlebelgasayofiint =4
    /**მეტი სისუფთავისთვის წილადების გამრავლების შემდეგ, ვიყენებ უდიდეს გამყოფზე შეკვეცის ფუნქციას და წილადს თან
     * შეკვეცილსაც ვიღებ. იგივენაირად ვშვები რიცხვზე გამრავლებისასაც */
    println("გავამრავლოთ წილადი 2/5 წილად 3/15ზე და შევკვეცოთ უ.ს.გ-ზე:")
    println(wiladigamravgayof1.wiladisGamravlebaWiladze(wiladigarmravgayof2))
    println("გავამრავლოთ წილადი 2/5, ამჯერად რიცხვ 4ზე და შევკვეცოთ უ.ს.გ-ზე:")
    println(wiladigamravgayof1.wiladisGamravlebaricxvze(gasamravlebelgasayofiint))

    /** ანალოგიურად გავაკეთეგაყოფაზეც */
    println("გავყოთ 2/5  3/15ზე და შევკვეცოთ უ.ს.გ-ზე:")
    println(wiladigamravgayof1.wiladisgayofawiladze(wiladigarmravgayof2))
    println("გავყოთ 2/5 ინთეჯერ ტიპის რიცხვზე, ამ შემთხვევაში 5-ზე:")
    println(wiladigamravgayof1.wiladisgayofaricxvze(5))

    /** ასევე გავაკეთე ფუნქცია, თუ სურვილისამებრ მენდომება წილადის რაიმე რიცხვზე შეკვეცა ,
     * რომელიც მხოლოდ იმ შემთხვევაში შეკვეცავს წილადს, როცა ეს შესაძლებელია*/
    println("შევკვეცოთ სასურველი წილადი სასურველ რიცხვზე. მაგ; 8/80 2-ზე:")
    println( wiladikvecadi.shevkvecotwiladiricxvze(2))

}





open class Point(
    private val x:Double,
    private val y:Double){
    override fun toString():String{
        return x.toString()+","+y.toString()
    }
    /** ოვერრაიდი გავუკეთე იქუალსის ზოგად ფუნქციას და ასე გავწერე, რადგან ობიექტების ადგილები მეხსიერებაში განსხვავებულია, სულ ფოლსს
     * მოგვცემდა და ამ შემთხვევაში, მათ მნიშვნელობებს ვადარებთ. თუ ორივე ფოინთის იქსი და იგრეკი ერთ ადგილზეა, მაშინ დისტანცია 0
     * იქნება და მათი მნიშვნელობები ტოლი. ისე მოვიქეცი, როგორც თქვენ ფრექშენის შემთხვევაში*/
    override fun equals(other: Any?): Boolean {
        if (findDistanceTo(other as Point)==0.0){
            return true
        }
        return false
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
    fun symetrivWithZero(): Point{
        val x=-x
        val y=-y
        return Point(x,y)
    }
    fun findDistanceTo(other: Point): Double {
        val pirveli = (x - other.x) * (x - other.x)
        val meore = (y - other.y) * (y - other.y)
        val jami = pirveli + meore
        return sqrt(jami)
    }
}

open class Fraction(private var numerator: Int,private var denominator: Int ) {
    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            if (numerator * other.denominator / denominator == other.numerator) {
                return true
            }
        }
        return false
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }

    override fun toString(): String {
        return "${this.numerator}/${this.denominator}"
    }

    open fun findusg(k1: Int,k2:Int):Int {
        if (k1==0){return k2}
        return this.findusg(k2%k1,k1)

    }


    open fun sum(other: Fraction): Fraction {
        if (this.denominator == other.denominator) {
            val sumofnuminators = this.numerator + other.numerator
            val gamyofi = this.denominator
            return Fraction(sumofnuminators, gamyofi)
        } else {

            val udsagam=findusg(this.denominator,other.denominator)
            if (this.denominator>other.denominator){
                val mnishvneli=(other.denominator/udsagam)*this.denominator
                val mricxveli=(other.numerator*this.denominator)/udsagam+this.numerator
                return Fraction(mricxveli,mnishvneli)
            }else
            {
                val mnishvneli=(this.denominator/udsagam)*other.denominator
                val mricxveli=(this.numerator*other.denominator)/udsagam+other.denominator
                return Fraction(mricxveli,mnishvneli)
            }

        }
    }


    open fun shekvecaudidesGamyofze():Fraction{
        val udidesisaertogamyofi=findusg(this.denominator,this.numerator)
        return Fraction(this.numerator/udidesisaertogamyofi,this.denominator/udidesisaertogamyofi)
    }

    open fun wiladisGamravlebaricxvze(n:Int):Fraction{
        val axaliwiladi=Fraction(this.numerator*n,this.denominator)
        return axaliwiladi.shekvecaudidesGamyofze()

    }
    open fun wiladisGamravlebaWiladze(other: Fraction):Fraction{
        val axaliwiladi=Fraction(this.numerator*other.numerator,this.denominator*other.denominator)
        return axaliwiladi.shekvecaudidesGamyofze()
    }

    open fun wiladisgayofawiladze(other: Fraction):Fraction{
        val axaliwiladi=Fraction(this.numerator*other.denominator,this.denominator*other.numerator)
        return axaliwiladi.shekvecaudidesGamyofze()
    }

    open fun wiladisgayofaricxvze(n:Int):Fraction{
        val axaliwiladi=Fraction(this.numerator,this.denominator*n)
        return axaliwiladi.shekvecaudidesGamyofze()
    }
    open fun shevkvecotwiladiricxvze(n:Int):Any{
        if (this.denominator%n==0 && this.numerator%n==0){
            val newmricxveli=this.numerator/n
            val newmnishvneli=this.denominator/n
            return Fraction(newmricxveli,newmnishvneli)
        }
        return "ამ წილადის შეკვეცა მოცემულ რიცხვზე შეუძლებელია"
    }
}

